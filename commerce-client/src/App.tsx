import React from 'react';
import './App.css';
import {Container, Typography} from "@material-ui/core";
import {CommerceTable} from "./components/CommerceTable";
import {OrderResponse} from "./interfaces/OrderResponse";
import {getOrdersFromApi} from "./services/orderService";
import {CommerceTableRow} from "./components/CommerceTableRow";
import {SearchParams} from "./interfaces/SearchParams";
import {SortOrderType} from "./interfaces/SortOrderType";
import {CommerceOrderCard} from "./components/CommerceOrderCard";

function App() {
    const defaultSearchParams: SearchParams = { page: 1, sortOrder: 'asc', numPerPage: 25}
    const [ orderResponse , setOrderResponse ] = React.useState<OrderResponse | null>(null);
    React.useEffect(() => {
        void getData(defaultSearchParams);
    }, [])
    const getData = async (searchParams: SearchParams) => {
        const data = await getOrdersFromApi(searchParams)
        setOrderResponse(data);
    }

    const handleParamsChange = (newPage: number, newRowsPerPage: number, sortOrder: SortOrderType) => {
        const newSearchParams: SearchParams = {
            page: newPage,
            numPerPage: newRowsPerPage,
            sortOrder: sortOrder
        }
        void getData(newSearchParams);
    }
  return (
    <Container>
      <Typography>Commerce Orders</Typography>
      <CommerceTable
          page={orderResponse?.page ? orderResponse.page - 1 : 1}
          numProductsPerPage={orderResponse?.resultsPerPage || 1}
          totalOrders={orderResponse?.totalNumberOfOrders || 1}
          sortOrder={orderResponse?.sortOrder || 'asc'}
          onParameterChange={handleParamsChange}
      >
          {orderResponse?.orders?.map(order => <CommerceTableRow key={`order_${order.id}`}
                                                                         transaction={order}
                                                                 shouldHighlightRow={order.orderTotal > orderResponse?.orderAverage}/>) || null}
      </CommerceTable>
        {orderResponse && (
            <React.Fragment>
                <CommerceOrderCard transaction={orderResponse?.mostExpensiveOrder} title={"Most Expensive Order"} />
                <CommerceOrderCard transaction={orderResponse?.leastExpensiveOrder} title={"Least Expensive Order"} />
            </React.Fragment>
        )}

    </Container>
  );
}

export default App;
