import {OrderResponse} from "../interfaces/OrderResponse";
import {SearchParams} from "../interfaces/SearchParams";


export const getOrdersFromApi = async (params: SearchParams): Promise<OrderResponse> => {
    const url = 'https://localhost:5001/api/orders'
    const search = `?sortOrder=${params.sortOrder}&page=${params.page}&rowsPerPage=${params.numPerPage}`
    const data = await fetch(url + search);
    return await data.json();
}