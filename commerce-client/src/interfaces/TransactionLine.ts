import {Product} from "./Product";

export interface TransactionLine {
    id: number
    transactionId: number
    productId: number
    quantity: number
    product: Product
}