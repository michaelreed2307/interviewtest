import {SortOrderType} from "./SortOrderType";

export interface SearchParams {
    page: number
    sortOrder: SortOrderType
    numPerPage: number
}