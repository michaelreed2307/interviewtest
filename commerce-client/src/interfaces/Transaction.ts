import {TransactionLine} from "./TransactionLine";

export interface Transaction {
    id: number
    firstName: string
    lastName: string
    email: string
    gender: string
    address: string
    postalCode: string
    orderTotal: number
    transactionLines: TransactionLine[]
}