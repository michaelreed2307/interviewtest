import {Transaction} from "./Transaction";
import {SortOrderType} from "./SortOrderType";

export interface OrderResponse {
    orderAverage: number
    totalNumberOfOrders: number
    resultsPerPage: number
    page: number
    sortOrder: SortOrderType
    orders: Transaction[]
    mostExpensiveOrder: Transaction
    leastExpensiveOrder: Transaction
}