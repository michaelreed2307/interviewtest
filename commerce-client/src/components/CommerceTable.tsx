import React from 'react';
import {
    IconButton,
    makeStyles, Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@material-ui/core";
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import {SortOrderType} from "../interfaces/SortOrderType";
interface CommerceTableProps {
    page: number
    numProductsPerPage: number
    totalOrders: number
    sortOrder: SortOrderType
    onParameterChange(newPage: number, newRowsPerPage: number, sortOrder: 'asc' | 'desc'): void;
}



export const CommerceTable:React.FC<CommerceTableProps> = (props) => {
    const {page , numProductsPerPage, totalOrders, sortOrder, children, onParameterChange} = props;
    const columns: string[] = ['', 'Id', 'Full Name', 'Email', 'Address', 'Postal Code', 'Order Total'];
    const classes = useStyles();

    const handleChangePage = (event: unknown, newPage: number) => {
        onParameterChange(newPage + 1, numProductsPerPage, sortOrder)
    }
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        onParameterChange(1, +event.target.value, sortOrder)
    }
    const handleSortOrderChange = () =>
        onParameterChange(page + 1, numProductsPerPage, sortOrder === 'asc' ? 'desc' : 'asc');

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column, idx) => (
                                <TableCell
                                    key={`column_${column}`}
                                >
                                    {idx == 0 ? (<IconButton size="small" onClick={handleSortOrderChange}>
                                        {sortOrder === 'asc' ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                    </IconButton>) : column}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {children}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[25, 50, 100]}
                component="div"
                count={totalOrders || 0}
                rowsPerPage={numProductsPerPage || 0}
                page={page || 0}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    )
}

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 440,
    },
});