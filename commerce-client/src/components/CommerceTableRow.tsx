import React from 'react';
import {
    Box,
    Collapse,
    IconButton, makeStyles,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography
} from "@material-ui/core";
import {Transaction} from "../interfaces/Transaction";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {formatCurrency} from "../helpers/formatCurrency";

interface CommerceTableRowProps {
    transaction: Transaction
    shouldHighlightRow: boolean
}

export const CommerceTableRow:React.FC<CommerceTableRowProps> = ({ transaction, shouldHighlightRow }) => {
    const classes = useStyles();
    const [ open , setOpen ] = React.useState<boolean>(false);
    return (
        <React.Fragment>
        <TableRow className={`${classes.root} ${shouldHighlightRow ? classes.greenBackground : ''}`}>
            <TableCell>
                <IconButton size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
            </TableCell>
            <TableCell component="th" scope="row">
                {transaction.id}
            </TableCell>
            <TableCell align="left">{transaction.firstName + ' ' + transaction.lastName}</TableCell>
            <TableCell align="left">{transaction.email}</TableCell>
            <TableCell align="left">{transaction.address}</TableCell>
            <TableCell align="left">{transaction.postalCode}</TableCell>
            <TableCell align="left">{formatCurrency(transaction.orderTotal)}</TableCell>
        </TableRow>
    <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={7}>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <Box margin={1}>
                    <Typography variant="h6" gutterBottom component="div">
                        Order Lines
                    </Typography>
                    <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>Quantity</TableCell>
                                <TableCell align="right">Amount</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {transaction.transactionLines.map((line) => (
                                <TableRow key={`line_${line.id}`}>
                                    <TableCell component="th" scope="row">
                                        {line.product.name}
                                    </TableCell>
                                    <TableCell>{line.quantity}</TableCell>
                                    <TableCell align="right">{formatCurrency(line.product.price)}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Box>
            </Collapse>
        </TableCell>
    </TableRow>
        </React.Fragment>
    )
}

const useStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
    greenBackground: {
        background: 'lightgreen'
    }
});