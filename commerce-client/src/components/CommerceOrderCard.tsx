import React from 'react';
import {Transaction} from "../interfaces/Transaction";
import {Card, CardContent, makeStyles, Typography} from "@material-ui/core";
import {formatCurrency} from "../helpers/formatCurrency";

interface CommerceOrderCardProps {
    transaction: Transaction
    title: string
}

export const CommerceOrderCard: React.FC<CommerceOrderCardProps> = ({ transaction , title}) => {
    const classes = useStyles();
    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {title}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    {formatCurrency(transaction.orderTotal)}
                </Typography>
                <Typography variant="body2" component="p">{`${transaction.firstName} ${transaction.lastName}`}</Typography>
                <Typography variant="body2" component="p">{transaction.email}</Typography>
                <Typography variant="body2" component="p">{transaction.address}</Typography>
                {transaction.postalCode && <Typography variant="body2" component="p">{transaction.postalCode}</Typography>}
            </CardContent>
        </Card>
    )
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});