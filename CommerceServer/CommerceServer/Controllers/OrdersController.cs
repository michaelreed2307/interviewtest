using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommerceServer.DAL;
using CommerceServer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CommerceServer.Controllers
{
    /// <summary>
    /// Gets orders from database
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        /// <summary>
        /// Represents the number of orders to return from the API
        /// </summary>
        public OrdersController(ApplicationDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// GET: api/Orders
        /// Gets orders from database.
        /// TODO: Implement query to retrieve required data from database
        /// TODO: Implement Order Response and return data as required
        /// </summary>
        /// <param name="sortOrder">Specifies the sort order of the data to be returned</param>
        /// <param name="page">Specifies which page to return</param>
        /// <returns>Restful order response</returns>
        [HttpGet]
        public async Task<OrderResponse> Get([FromQuery] string sortOrder = "asc", [FromQuery] int page = 1, [FromQuery] int rowsPerPage = 25)
        {
            var transactions = await GetTopTransactions(sortOrder, page, rowsPerPage);
            return ProcessOrders(transactions, page, rowsPerPage, sortOrder);
        }

        /// <summary>
        /// Processes order data
        /// TODO: Calculate the total for each order retrieved
        /// TODO: Calculate the order average from the result set
        /// TODO: Find the least expensive order in the result set
        /// TODO: Find the most expensive order in the result set
        /// TODO: Find the total number of orders in the database
        /// </summary>
        /// <param name="transactions">Current set of transactions from the database</param>
        /// <param name="page">Current page</param>
        /// <param name="rowsPerPage">Number of rows to return</param>
        /// <param name="sortOrder">Defines the sort for the results</param>
        /// <returns>Order response object with all properties filled</returns>
        private OrderResponse ProcessOrders(IReadOnlyCollection<Transaction> transactions, int page, int rowsPerPage, string sortOrder)
        {
            foreach (var transaction in transactions)
            {
                transaction.OrderTotal = transaction.TransactionLines.Sum(ol => ol.Quantity * ol.Product.Price);
            }
            var orderResponse = new OrderResponse()
            {
                Orders = transactions,
                OrderAverage = Math.Round(transactions.Average(o => o.OrderTotal), 2),
                LeastExpensiveOrder = transactions.OrderBy(o => o.OrderTotal).First(),
                MostExpensiveOrder = transactions.OrderByDescending(o => o.OrderTotal).First(),
                TotalNumberOfOrders = _context.Customer.Count(),
                ResultsPerPage = rowsPerPage,
                Page = page,
                SortOrder = sortOrder
            };
            return orderResponse;
        }

        /// <summary>
        /// Gets transactions from the database within the requested parameters
        /// </summary>
        /// <param name="sortOrder">Specified sort order from request</param>
        /// <param name="page">Specified page from request</param>
        /// <param name="rowsPerPage">Number of rows to return</param>
        /// <returns>Collection of transactions</returns>
        private async Task<IReadOnlyCollection<Transaction>> GetTopTransactions(string sortOrder, int page, int rowsPerPage)
        {
            var query = _context.Customer.Include("TransactionLines.Product");
            query = sortOrder == "asc" ? query.OrderBy(c => c.LastName) : query.OrderByDescending(c => c.LastName);
            return await query.Skip(page * rowsPerPage).Take(rowsPerPage).ToListAsync();
        }
    }
}
