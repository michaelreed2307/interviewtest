const sqlite3 = require('sqlite3');
const { open } = require('sqlite');

async function main() {
    const db = await createDbConnection('commerce.db')
    const products = await getProducts(db);
    const customers = await getCustomers(db);
    await dropOrders(db);
    await createOrders(customers, products, db);
}

async function dropOrders(db) {
    try {
        await db.run('DROP TABLE order_line')
    } catch (err) {
        console.error(err);
    }
}

async function getProducts(db) {
    try {
        const query = 'select * from product';
        const products = await db.all(query)
        return products
    } catch (err) {
        console.error(err);
    }
}

async function getCustomers(db) {
    try {
        const query = 'select * from customer';
        const customers = await db.all(query);
        return customers;
    } catch (err) {
        console.error(err);
    }
}

async function createOrders(customers, products, db) {
    const orders = [];
    let orderId = 1;
    customers.forEach(cust => {
        const numProducts = Math.floor(Math.random() * 4) + 1;
        for (let i = 0; i < numProducts; i++) {
            const randomProduct = Math.floor(Math.random() * products.length);
            const qty = Math.floor(Math.random() * 3) + 1;
            orders.push({id: orderId, customer_id: cust.id, product_id: products[randomProduct].id, quantity: qty})
            orderId++;
        }

    })
    await insertOrders(db, orders);
}

async function insertOrders(db, orders) {
    await db.run(`CREATE TABLE order_line (
        id INT,
        customer_id INT,
        product_id INT,
        quantity INT
        );
`)
    orders.forEach(order => {
        db.run(`INSERT INTO order_line VALUES (${order.id} , ${order.customer_id} , ${order.product_id}, ${order.quantity});`)
    })
}

function createDbConnection(filename) {
    return open({
        filename,
        driver: sqlite3.Database
    });
}

main().then(() => console.log('Orders Generated'));